_ = require('lodash')

var DEBUG = !!process.env.DEBUG
console.log("DEBUG: ", DEBUG)

function debug(...args) {
  if (DEBUG) console.log(...args)
}

function parseParams(str) {
  //spec is a string describing the message parameters, possibly including a host
  let [params,recipient] = str.split('->').map(s => s.trim())
  let parameters = params.split(',').map(s => s.trim()).map(s => s.split(' '))
  let spec = {"keys":[], "ins":[], "outs":[], "nils":[], "any": [], "parameters":[], "recipient":recipient}
  for (const p of parameters) {
    if (['in', 'out', 'nil', 'any'].includes(p[0])) {
      var name = p[1]
    } else {
      var name = p[0]
    }

    if (spec.parameters.includes(name)) {
      continue
    }

    spec.parameters.push(name)

    if (p.includes("key")) { spec.keys.push(name) }

    if (p.includes("out")) { spec.outs.push(name) }
    else if (p.includes("in")) { spec.ins.push(name) }
    else if (p.includes("nil")) { spec.nils.push(name) }
  }
  return spec
}

function parseSpec(str) {
  lines = str.split('\n')
  messages = []
  for (var l of lines) {
    messages.push(parseParams(l))
  }
  return messages
}

function getPredecessors(keys, history, payload) {
  // find relevant predecessors in history
  preds = []
  for (var k of keys) {
    if (payload[k] !== undefined) {
      // only filter by keys that appear in the message
      if (k in history && !preds.length) {
        // get all messages that match on the first key
        preds = history[k][payload[k]] || []
      } else if (k in history) {
        // filter by the rest of the keys -- shouldn't be many left
        preds = preds.filter(m => m[k] == payload[k])
      }
    }
    if (!preds.length) return preds
  }
  return preds
}

function bindings(param, preds) {
  if (preds == undefined || preds.length == 0) return []
  return preds.filter(m => param in m).map(m => m[param])
}

function getEnactment(keys, history, payload) {
  matches = {}
  for (var k of keys) {
    if (payload[k] !== undefined) {
      matches[k] = (history[k][payload[k]] || []).map(m => _.omitBy(m, (v,k)=>k[0]=='_'))
    } else {
      matches[k] = []
    }
  }

  enactment = {history: matches}

  preds = matches[keys[0]]
  for (var k of keys.slice(1)) {
    preds = preds.filter(m => m[k] == payload[k])
  }

  enactment.bindings = preds.reduce((acc, curr) => Object.assign(acc, curr), {})

  return enactment
}

function observe(spec, history, payload) {
  msg = _.clone(payload)
  msg._timestamp = Date.now()
  for (var p of Object.keys(msg)) {
    if (p in history.parameters) {
      history.parameters[p].add(msg[p])
    } else {
      history.parameters[p] = new Set([msg[p]])
    }
  }
  for (var k of spec.keys) {
    let v = msg[k]
    if (v !== undefined) {
      if (history[k] !== undefined) {
        if (history[k][v] !== undefined) {
          // add message to existing index for key
          history[k][v].push(msg)
        } else {
          // add new key to existing index
          history[k][v] = [msg]
        }
      } else {
        // initialize new index if msg has this key
        history[k] = { [v]: [msg] }
      }
    }
  }
}

module.exports = { DEBUG, debug, parseSpec, parseParams, getEnactment, observe, bindings, getPredecessors}
