#!/bin/zsh
setopt extendedglob

python ../average_results.py *mqtt-<1-5>.log >protocol-mqtt-0%
python ../average_results.py *udp-<1-5>.log >protocol-udp-0%
python ../average_results.py *retry-<1-5>.log >protocol-retry-0%

losses=(10 20 30 40 50 60 70 80 90 95 99)
for i in $losses; do
    python ../average_results.py *mqtt-${i}\%-<1-5>.log >protocol-mqtt-${i}\%
    python ../average_results.py *udp-${i}\%-<1-5>.log >protocol-udp-${i}\%
    python ../average_results.py *retry-${i}\%-<1-5>.log >protocol-retry-${i}\%
done

rm results.csv
for f in $(ls *%); do
    paste -d"," <(echo $f) <(tail -n1 $f | cut -d"," -f2,3) >>results.csv
done
