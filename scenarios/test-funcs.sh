#!/bin/zsh

results_file=${1:-results}

beep() {
  for i in {1..4}; do
      play -q -n synth 0.1 sin 440 2>/dev/null
  done
}

flow_pid=$(mktemp)
start_flow() {
    sudo -u samuel DEBUG=true MESSAGE_COPIES=$MESSAGE_COPIES node-red ${1} >node-red.log &
    sleep 1
    ps --ppid $! -o pid= >${flow_pid}
    sleep 5
}

stop_flow() {
    kill -9 $(cat ${flow_pid})
}

add_loss() {
    echo "Adding ${1}% loss"
    tc qdisc add dev lo root netem loss ${1:-10}\%
}

remove_loss() {
    tc qdisc del dev lo root netem
}

run_test() {
    echo Running tests
    sudo -u samuel python run-test.py --max-duration $(( 60 )) "${@:1}" #>>${results_file}
}

start_broker() {
    docker run --rm -d --name broker -p 1883:1883 eclipse-mosquitto
}

stop_broker() {
    docker kill broker
    sleep 1
}

test_loop() {
    flow=$1
    name=${@:2}

    if (( loss > 0 )); then
        name="${name}-${loss}%"
    fi

    for i in {1..5}; do
        if [[ -v broker ]]; then
            start_broker
        fi
        if (( loss > 0 )); then
            add_loss $loss
        fi
        start_flow $flow

        sleep 65

        stop_flow

        if (( loss > 0 )); then
            remove_loss
        fi
        if [[ -v broker ]]; then
            stop_broker
        fi
        mv results.log "results/${name}-${i}.log"
    done

    beep
}

loss_loop() {
    for i in $(seq 0 10 90); do
        if (( $i > 0 )); then
            loss=$i test_loop "$@"
        else
            test_loop "$@"
        fi
    done
}
