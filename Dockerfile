FROM nodered/node-red:latest-minimal

USER root

RUN apk add --no-cache iproute2
RUN npm install node-red-contrib-uuid
RUN npm install node-red-contrib-mqtt-broker
RUN npm install node-red-contrib-wait-paths
COPY node-red-contrib-PoT /src/node-red-contrib-PoT
RUN npm install /src/node-red-contrib-PoT

USER node-red